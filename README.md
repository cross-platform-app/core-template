# Cross-platform-app: Core

This is a template/demo that shows how to build a platform-independent codebase for the business-logic of an application, written entirely in C++. The library supports bindings for Java, Objective-C and Javascript, so it can be used on nearly every platform, including Windows, Linux, macOS, iOS, Android and the Browser.

This repository only shows the platform-independent part of the code. To see how to use this library on any of the listed platforms, have a look at the related repositories in the [cross-platform-app](https://gitlab.com/cross-platform-app) gitlab group.

## Build Dependencies
- cmake >= 3.7.1
- doxygen >= 1.8 *(optional)*
- jre <= 1.8 (>1.8 [will not work](https://github.com/dropbox/djinni/issues/356)!)

## Build Instructions
The CMake-Project offers different targets, depending on the configuration:
- `Core--Cpp` (alias: `Core::Cpp`) builds a static library to be included in a *C++*-Project
- `Core--ObjC` (alias: `Core::ObjC`) builds a static library for use in *Objective-C*
- `Core--JNI` (alias: `Core::JNI`) builds a dynamic library for use in Java-Projects. This will also export the variable `Core--JAVA_SOURCES` to the parent scope, which contains a list of all the generated Java-files that are needed to talk to the native code.
- `Core--emscripten` (no alias) builds an executable for the browser (Javascript). This target is meant to be used for cross-platform-compilation with the Emscripten-toolchain.

The targets are only available if the correct value is set to `TARGET_PLATFORM`.
Available values are: `JAVA`, `WEB`, `MACOS` and `IPHONE`.
If you don't set any of these values, then only `Core::Cpp` is available.

### Building for Emscripten
To build the `Core--emscripten` target for the browser, run CMake with the provided toolchain-file from [here](https://github.com/kripken/emscripten/blob/master/cmake/Modules/Platform/Emscripten.cmake)
A copy of the file can also be found in this repository.
```
cmake -H. -B_builds -DCMAKE_TOOLCHAIN_FILE=cmake/Emscripten.cmake -G "Unix Makefiles" -DTARGET_PLATFORM="WEB" -DCMAKE_C_ABI_COMPILED=ON -DCMAKE_CXX_ABI_COMPILED=ON
```
Don't forget to set the flags `-DCMAKE_C_ABI_COMPILED=ON` and `-DCMAKE_CXX_ABI_COMPILED=ON`. They are needed to make Hunter work with the Toolchain.

## Generate documentation
Generate html documentation with the target `Core--doc`.

The target is only available if a matching version of doxygen could be found on the system (see requirements). 

The output can be found under `doc/html`
