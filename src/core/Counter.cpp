#include "Counter.hpp"
#include "config.h"

namespace core {
    int32_t Counter::getNumber() {
        return _counter;
    }

    void Counter::resetNumber() {
        _counter = 0;
        signalOnNumberChangedSuccess();
    }

    void Counter::increaseNumber() {
        if (_counter < 10) {
            _counter++;
            signalOnNumberChangedSuccess();
        }
        else {
            // this preprocessor-replacement has been defined in config.h
            // chooses correct function for thread-timeout according to the target-platform
            THREAD_SLEEP(3000);
            signalOnNumberChangedError();
        }
    }

    void Counter::onNumberChanged(const std::shared_ptr<OnChangeListener> &onChangeListener) {
        signalOnNumberChangedSuccess.connect([onChangeListener](){onChangeListener->onSuccess();});
        signalOnNumberChangedError.connect([onChangeListener](){onChangeListener->onError();});
    }
}

