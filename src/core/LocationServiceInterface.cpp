#include "core/LocationServiceInterface.hpp"
#include "LocationService.hpp"

namespace core {
    std::shared_ptr<LocationServiceInterface> LocationServiceInterface::LocationService(const std::shared_ptr<LocationInterface> & locator) {
        return std::shared_ptr<LocationServiceInterface>(new core::LocationService(locator));
    }
}