#include "LocationService.hpp"
#include "core/OnChangeListener.hpp"
#include <thread>
#include <chrono>

namespace core {

class OnLocationUpdateListener : public OnChangeListener {
public:
    OnLocationUpdateListener(LocationService * service) {
       _service = service;
    };
    void onSuccess() {
        _service->signalOnLocationChangedSuccess();
    }
    void onError() {
        _service->signalOnLocationChangedError();
    }
private:
    LocationService * _service;
};

LocationService::LocationService(const std::shared_ptr<LocationInterface> & locator)
{
    _locator = locator;
}

LocationRecord LocationService::getLocation() {
    return _locator->getLocation();
}

void LocationService::updateLocation() {
    _locator->updateLocation(std::shared_ptr<core::OnChangeListener>(new OnLocationUpdateListener(this)));
}

void LocationService::onLocationChanged(const std::shared_ptr<OnChangeListener> & onChangeListener) {
    signalOnLocationChangedSuccess.connect([onChangeListener](){onChangeListener->onSuccess();});
    signalOnLocationChangedError.connect([onChangeListener](){onChangeListener->onError();});
}

}