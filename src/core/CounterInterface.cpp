#include "core/CounterInterface.hpp"
#include "Counter.hpp"

namespace core {
    std::shared_ptr<CounterInterface> CounterInterface::Counter() {
        return std::shared_ptr<CounterInterface>(new core::Counter);
    }
}