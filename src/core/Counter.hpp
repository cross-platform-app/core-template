#pragma once

#include "core/CounterInterface.hpp"
#include "core/OnChangeListener.hpp"
#include "boost/signals2.hpp"

namespace core {
    class Counter : public CounterInterface {
    public:
        int32_t getNumber() override;
        void resetNumber() override;
        void increaseNumber() override;
        void onNumberChanged(const std::shared_ptr<OnChangeListener> & onChangeListener) override;

        boost::signals2::signal<void (void)> signalOnNumberChangedSuccess;
        boost::signals2::signal<void (void)> signalOnNumberChangedError;
    private:
        int32_t _counter = 0;

    };
}

