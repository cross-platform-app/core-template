#pragma once

#include "core/LocationServiceInterface.hpp"
#include "core/LocationInterface.hpp"
#include "core/LocationRecord.hpp"
#include "core/OnChangeListener.hpp"
#include <memory>
#include "boost/signals2.hpp"

namespace core {

class LocationService : public LocationServiceInterface {
public:

    LocationService(const std::shared_ptr<LocationInterface> & locator);

    LocationRecord getLocation() override;

    void updateLocation() override;

    void onLocationChanged(const std::shared_ptr<OnChangeListener> & onChangeListener) override;

    boost::signals2::signal<void (void)> signalOnLocationChangedSuccess;
    boost::signals2::signal<void (void)> signalOnLocationChangedError;


private:
    std::shared_ptr<LocationInterface> _locator;
};

}  // namespace core
