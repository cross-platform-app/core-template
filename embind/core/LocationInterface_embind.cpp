#include "core/LocationInterface.hpp"
#include "core/LocationRecord.hpp"
#include "core/OnChangeListener.hpp"

#include <emscripten/bind.h>

using namespace emscripten;

struct LocationInterfaceWrapper : public wrapper<core::LocationInterface> {
    EMSCRIPTEN_WRAPPER(LocationInterfaceWrapper);
    void updateLocation(const std::shared_ptr<core::OnChangeListener> & onChangeListener) {
        return call<void>("updateLocation", onChangeListener);
    };

    core::LocationRecord getLocation() {
        return call<core::LocationRecord>("getLocation");
    };
};

EMSCRIPTEN_BINDINGS(locationInterface) {
        class_<core::LocationInterface>("LocationInterface")
                .smart_ptr<std::shared_ptr<core::LocationInterface>>("std::shared_ptr<core::LocationInterface>")
                .function("updateLocation", &core::LocationInterface::updateLocation, pure_virtual())
                .function("getLocation", &core::LocationInterface::getLocation, pure_virtual())
                .allow_subclass<LocationInterfaceWrapper>("LocationInterfaceWrapper")
        ;
}