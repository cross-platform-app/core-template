#include "core/LocationRecord.hpp"

#include <emscripten/bind.h>

using namespace emscripten;

EMSCRIPTEN_BINDINGS(locationRecord) {
        class_<core::LocationRecord>("LocationRecord")
                .constructor<float, float>()
                .property("lat", &core::LocationRecord::lat)
                .property("lon", &core::LocationRecord::lon)
                ;
}