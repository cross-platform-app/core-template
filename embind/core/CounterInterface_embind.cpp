#include "core/CounterInterface.hpp"

#include <emscripten/bind.h>

using namespace emscripten;

EMSCRIPTEN_BINDINGS(counter) {
        class_<core::CounterInterface>("CounterInterface")
                .smart_ptr<std::shared_ptr<core::CounterInterface>>("std::shared_ptr<core::CounterInterface>")
                .class_function("Counter", &core::CounterInterface::Counter)
                .function("increaseNumber", &core::CounterInterface::increaseNumber)
                .function("resetNumber", &core::CounterInterface::resetNumber)
                .function("getNumber", &core::CounterInterface::getNumber)
                .function("onNumberChanged", &core::CounterInterface::onNumberChanged)
                ;
}
