#include "core/OnChangeListener.hpp"

#include <emscripten/bind.h>

using namespace emscripten;

struct OnChangeListenerWrapper : public wrapper<core::OnChangeListener> {
    EMSCRIPTEN_WRAPPER(OnChangeListenerWrapper);
    void onSuccess() {
            return call<void>("onSuccess");
    }
    void onError() {
        return call<void>("onError");
    }

};


EMSCRIPTEN_BINDINGS(onChangeListener) {
        class_<core::OnChangeListener>("OnChangeListener")
                .smart_ptr<std::shared_ptr<core::OnChangeListener>>("std::shared_ptr<core::OnChangeListener>")
                .function("onSuccess", &core::OnChangeListener::onSuccess, pure_virtual())
                .function("onError", &core::OnChangeListener::onError, pure_virtual())
                .allow_subclass<OnChangeListenerWrapper>("OnChangeListenerWrapper")
        ;
}
