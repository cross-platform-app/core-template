#include "core/LocationServiceInterface.hpp"

#include <emscripten/bind.h>
#include "LocationRecord_embind.cpp"

using namespace emscripten;

EMSCRIPTEN_BINDINGS(locationService) {
        class_<core::LocationServiceInterface>("LocationServiceInterface")
                .smart_ptr<std::shared_ptr<core::LocationServiceInterface>>("std::shared_ptr<core::LocationServiceInterface>")
                .class_function("LocationService", &core::LocationServiceInterface::LocationService)
                .function("updateLocation", &core::LocationServiceInterface::updateLocation)
                .function("getLocation", &core::LocationServiceInterface::getLocation)
                .function("onLocationChanged", &core::LocationServiceInterface::onLocationChanged)
        ;
}